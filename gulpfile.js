var gulp = require('gulp'),
    browserSync = require('browser-sync').create(),
    gulpIf = require('gulp-if'),
    sass = require('gulp-sass'),
    autoprefixer = require('gulp-autoprefixer'),
    concat = require('gulp-concat'),
    minifyCss = require('gulp-minify-css'),
    uglify = require('gulp-uglify'),
    rename = require('gulp-rename'),
    sourcemaps = require('gulp-sourcemaps'),
    plumber = require('gulp-plumber'),
    gutil = require('gulp-util'),
    iconfont = require('gulp-iconfont'),
    connect = require('gulp-connect-php'),
    env = require('gulp-env'),
    iconfont = require('gulp-iconfont'),
    consolidate = require("gulp-consolidate"),
    spritesmith = require('gulp.spritesmith'),
    merge = require('merge-stream'),
    processhtml = require('gulp-processhtml'),
    header = require('gulp-header'),
    fs = require('fs');

var reload = browserSync.reload;
var pkg = JSON.parse(fs.readFileSync('./package.json'));
var banner = '/*! <%= new Date().toDateString() %> | <%= pkg.name %> | <%= pkg.description %> | <%= pkg.author %> */\n';

//environment variables
env({
    file: ".env.json"
});

var src = 'source',
    dest = 'public';

var env = {
    isProduction: false,
    autoprefixer: ['> 0%']
}

//serve php files
// gulp.task('php', function() {
//     connect.server({
//         base: dest,
//         port: process.env.PHPPORT
//     });
// });

gulp.task('serve', function() {
    browserSync.init({
        // proxy: process.env.PROXY,
        port: process.env.PORT,
        server: {
            baseDir: dest
        },
        notify: false,
        open: true,
    });

    gulp.watch(dest + '/images/icons/*.png', ['make:sprite']);
    gulp.watch(src + '/sass/**/*.scss', ['make:css']);
    gulp.watch(src + '/js/**/*js', ['make:app-js', 'make:js', 'make:plugin-js', 'make:ie8-js']);
    gulp.watch(src + '/js/**/*.js').on('change', reload);
    gulp.watch(dest + '/src/*.html', ['make:html']);
    gulp.watch([dest + '/src/component/*.html', dest + '/src/inc/*.html'], ['make:include-html']);
    // gulp.watch(dest + '/src/**/*.html').on('change', reload);

});

gulp.task('make:html', function(){
    var opts = { /* plugin options */ };
    return gulp.src(dest + '/src/**/*.html')
               .pipe(processhtml(opts))
               .pipe(gulp.dest(dest + '/'))
               .pipe(browserSync.stream());
});

gulp.task('make:include-html', function(){
    var opts = { /* plugin options */ };
    return gulp.src(dest + '/src/*.html')
               .pipe(processhtml(opts))
               .pipe(gulp.dest(dest + '/'))
               .pipe(browserSync.stream());
});

gulp.task('make:css', function() {
    return gulp.src(src + '/sass/**/*.scss')
        .pipe(plumber(function(error) {
            gutil.beep();
            gutil.log(gutil.colors.red(error.message));
            this.emit('end');
        }))
        .pipe(gulpIf(!env.isProduction, sourcemaps.init()))
        .pipe(sass())
        .pipe(autoprefixer(env.autoprefixer))
        .pipe(gulpIf(!env.isProduction, sourcemaps.write()))
        .pipe(gulpIf(env.isProduction, minifyCss()))
        .pipe(header(banner, {pkg: pkg}))
        .pipe(gulp.dest(dest + '/css'))
        .pipe(browserSync.stream());
});

gulp.task('make:plugin-css', function() {
    return gulp.src([
            src + '/css/plugin/jquery.fullPage.css',
            // src + '/css/plugin/jquery.bxslider.css',
        ])
        .pipe(concat('plugin.css'))
        .pipe(gulpIf(!env.isProduction, sourcemaps.init()))
        .pipe(gulpIf(!env.isProduction, sourcemaps.write()))
        .pipe(gulpIf(env.isProduction, minifyCss()))
        .pipe(header(banner, {pkg: pkg}))
        .pipe(gulp.dest(dest + '/css'))
        .pipe(browserSync.stream());
});

gulp.task('make:ie8-js', function() {
    return gulp.src([
            // src + '/js/plugin/respond.min.js',
            // src + '/js/plugin/html5shiv.js'

        ])
        .pipe(concat('ie8-fix.min.js'))
        .pipe(gulpIf(env.isProduction, uglify()))
        .pipe(header(banner, {pkg: pkg}))
        .pipe(gulp.dest(dest + '/js'));
});

gulp.task('make:plugin-js', function() {
    console.log(src+'/js/plugin/jquery.js');
    console.log(src + '/js/plugin/mobile-detect.min.js');
    return gulp.src([
            src + '/js/plugin/jquery.js',
            src + '/js/plugin/jquery.fullPage.js',
            // src + '/js/plugin/jquery.bxslider.js',
            src + '/js/plugin/GreenSock/TweenMax.min.js',
            src + '/js/plugin/GreenSock/TweenLite.min.js',
            src + '/js/plugin/GreenSock/TimenlineMax.min.js',
            src + '/js/plugin/GreenSock/TimelineLite.min.js',
            // src + '/js/plugin/mobile-detect.min.js',
        ])
        .pipe(concat('plugin.min.js'))
        .pipe(gulpIf(env.isProduction, uglify()))
        .pipe(header(banner, {pkg: pkg}))
        .pipe(gulp.dest(dest + '/js'));
});

gulp.task('make:app-js', function() {
    return gulp.src([
            // src + '/js/app.js',
            // src + '/js/controllers/*.js',
            // src + '/js/directives/*.js'
        ])
        .pipe(concat('all-app.js'))
        .pipe(header(banner, {pkg: pkg}))
        .pipe(gulp.dest(dest + '/js'));
});

gulp.task('make:js', function() {
    return gulp.src([
            src + '/js/main.js'
        ])
        .pipe(concat('main.min.js'))
        .pipe(gulpIf(env.isProduction, uglify()))
        .pipe(gulp.dest(dest + '/js'));
})

gulp.task('make:iconfont', function() {
    var fontName = 'icons';
    var template = 'icons-tpl';
    return gulp.src(['source/icons/*.svg'])
        .pipe(iconfont({
            fontName: fontName,
            normalize: true,
            fontHeight: 1001,
            formats: ['ttf', 'eot', 'woff']
        }))
        .on('glyphs', function(glyphs) {
            var options = {
                glyphs: glyphs.map(function(glyph) {
                    return {
                        name: glyph.name,
                        codepoint: glyph.unicode[0].charCodeAt(0)
                    }
                }),
                fontName: fontName,
                fontPath: '../fonts/',
                className: 'icons'
            };

            gulp.src(src + '/icons/template/' + template + '.css')
                .pipe(consolidate('lodash', options))
                .pipe(rename({ basename: fontName }))
                .pipe(header(banner, {pkg: pkg}))
                .pipe(gulp.dest(dest + '/css/'));

            gulp.src(src + '/icons/template/' + template + '.html')
                .pipe(consolidate('lodash', options))
                .pipe(rename({ basename: 'sample' }))
                .pipe(gulp.dest(dest + '/css/'));
        })
        .pipe(gulp.dest(dest + '/fonts/'));
});

gulp.task('make:sprite', function() {
    // console.log(dest + 'images/icons/*.png');
    var spriteData = gulp.src(dest + '/images/icons/*.png').pipe(spritesmith({
        imgName: 'sprite.png',
        cssName: '_sprites-icons.scss',
        cssFormat: 'scss',
        imgPath: '../images/sprite.png'
    }));

    var imgStream = spriteData.img.pipe(gulp.dest(dest + '/images/'));
    var cssStream = spriteData.css.pipe(gulp.dest(src + '/sass/sprites/'));
    return merge(imgStream, cssStream);
    // return imgStream;
});

//run gulp
gulp.task('default', ['make:sprite', 'make:css', 'make:plugin-css', 'make:app-js', 'make:js', 'make:plugin-js', 'make:ie8-js', 'make:iconfont', 'serve']);