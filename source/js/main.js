$(window).load(function() {
    // Animate loader off screen
    $(".se-pre-con").fadeOut("slow");
});

var now = 3;
var width, height, bg_move, area_move;
$(function() {
	// 背景放大1.2倍，已在css預設放大和位移，背景和前面section分開
	width = $(window).width();
	// console.log('width:'+width);
	begin_status();
	$(window).resize(function() {
		width = $(window).width();
		// console.log('width:'+width);
		begin_status();
	});
	

	// 首頁的繳費按鈕
	$('.mainCost').click(function(){
		// 離開首頁到section2，縮小＋位移
		// change_scaleAndPosition(leave, section, scale);
		change_scaleAndPosition(now, 2, 1);
	});
	$('.mainTax').click(function(){
		change_scaleAndPosition(now, 4, 1);
	});
	// 導覽列Menu
	$('.navCost').click(function(){

		change_scaleAndPosition(now, 2, 1);
	});
	$('.navTax').click(function(){
		change_scaleAndPosition(now, 4, 1);
	});
	$('.navHome').click(function(){
		change_scaleAndPosition(now, 3, 1.2);
	});

	//section arrow
	$('.s1_right').click(function(){
		change_scaleAndPosition(now, 2, 1);
	});
	$('.s2_left').click(function(){
		change_scaleAndPosition(now, 1, 1);
	});
	$('.s2_right').click(function(){
		change_scaleAndPosition(now, 3, 1.2);
	});
	$('.s4_left').click(function(){
		change_scaleAndPosition(now, 3, 1.2);
	});

	$('.main_item div').click(function(){
		$(this).find('ul').stop();

		$('.main_item div').removeClass('active');
		$('.main_item div ul').slideUp();
		if($(this).find('ul').css('display')=='none'){
			$(this).addClass('active');
			$(this).find('ul').slideDown();
		}else{
			$(this).removeClass('active');
			$(this).find('ul').slideUp();
		}
	});

});
function begin_status() {
	// 初始值 Home
	$('section').eq(2).addClass('active');
	if(width<1024){
		bg_move = -(1024*2)+(width-(1024*1.2))/2+22;
		area_move = -(1024*2)+(width-(1024))/2+22;
	}else{
		bg_move = -(1024*2)+(width-(1024*1.2))/2;
		area_move = -(1024*2)+(width-(1024))/2;
	}

	// console.log('bg_move:'+bg_move+"/ area_move:"+area_move);
	$('.all_bg.active').css('left', bg_move);
	$('.all_area.active').css('left', area_move);
	$('.BG_left').css('left', -512+((width-(1024))/2));
	$('.BG_right').css('left', ((width-1024)/2)+1024);


	TweenMax.to($('.cat'), 1, {scale: '1', left: '2450px', ease: Linear.easeNone});
	TweenMax.to($('.s3 .left'), 1, {delay:0.5, scale: '1', ease: Linear.easeNone});
	TweenMax.to($('.s3 .right'), 1, {delay:0.5, scale: '1', ease: Linear.easeNone});
	TweenMax.to($('.s3 .title'), 1, {delay:1, top: '-50', ease: Linear.easeNone});
	
}

function change_scaleAndPosition(leave, section, scale) {
		$(this).stop();
		// console.log('leave:'+leave);
		// console.log('section:'+section);
		if(leave!=section){
			// 位移多少
			if(width<1024){
				bg_move = -(1024*(section-1))+(width-(1024*scale))/2+22;
				area_move = -(1024*(section-1))+(width-(1024))/2+22;
			}else{
				bg_move = -(1024*(section-1))+(width-(1024*scale))/2;
				area_move = -(1024*(section-1))+(width-(1024))/2;
			}
			//還原狀態
			$('.cat').removeClass('walking_left');
			$('.cat').removeClass('walking_right');
			$('.cat').removeClass('active_cost');
			$('.cat').removeClass('active_tax');
			$('.topic_cost').removeClass('active');
			$('.topic_tax').removeClass('active');
			//背景縮小
			if(scale==1){
				$('.all_bg, .all_area').removeClass('active');
				//向左位移
				if(leave > section){
					if(leave==3 && section==2){
						//貓咪從section3向左走到section2
						$('.cat').addClass('walking_left');
						TweenMax.to($('.cat'), 2, {left: '1820px',top: '250px', ease: Linear.easeNone,
							onComplete: function(){
								$('.cat').removeClass('walking_left');
								$('.cat').addClass('active_cost');
								$('.topic_cost').addClass('active');
								$('.broad').css('left','1060px');
								TweenMax.to($('.broad'), 0.5, {delay: 0.5, bottom: '0px', ease: Linear.easeNone});
							}
						});
					}else if(leave==2 && section==1){
						//貓咪從section2向左走到section1
						$('.cat').addClass('walking_left');
						$('.broad').css('bottom','-180px');
						TweenMax.to($('.cat'), 2, {left: '800px',top: '250px', ease: Linear.easeNone,
							onComplete: function(){
								$('.cat').removeClass('walking_left');
								$('.cat').addClass('active_cost');
								$('.topic_cost').css('left','550px');
								$('.topic_cost').addClass('active');
								$('.broad').css('left','60px');
								TweenMax.to($('.broad'), 0.5, {delay: 0.5, bottom: '0px', ease: Linear.easeNone});
							}
						});

					}else if(leave==4 && section==2){
						//貓咪從section4向左走到section2
						
						$('.cat').addClass('walking_left');
						TweenMax.to($('.cat'), 2, { left: '1820px',top: '250px', ease: Linear.easeNone,
							onComplete: function(){
								$('.topic_cost').addClass('active');
								$('.cat').removeClass('walking_left');
								$('.cat').addClass('active_cost');
								TweenMax.to($('.topic_cost'), 0.3, {delay: 0.5, scale: '1', ease: Linear.easeNone});
								$('.broad').css('left','1060px');
								TweenMax.to($('.broad'), 0.5, {delay: 0.5, bottom: '0px', ease: Linear.easeNone});
							}
						});
					}
					

					//切換背景
					setTimeout(function(){
						$('.all_bg').css('left', bg_move);
						$('.all_area').css('left', area_move);
						$('section').removeClass('active');
						$('section').find('.arrow_control').css('opacity','0');
						$('section').eq(section-1).addClass('active');
						TweenMax.to($('section').eq(section-1).find('.arrow_control'), 0.5, {delay: 1.5, opacity: '1', ease: Linear.easeNone});
					}, 500);
					now = section;
					// console.log('now:'+now);
				}else if(leave < section){
					//往右位移
					$('.all_bg, .all_area').removeClass('active');
					//貓咪向右走
					if(leave==3 && section==4){
						$('.cat').addClass('walking_right');
						TweenMax.to($('.cat'), 2, {left: '3100px',top: '250px', ease: Linear.easeNone,
							onComplete: function(){
								$('.cat').removeClass('walking_right');
								$('.cat').addClass('active_tax');
								$('.topic_tax').addClass('active');
								$('.broad').css('left','3900px');
								TweenMax.to($('.broad'), 0.5, {delay: 0.5, bottom: '0px', ease: Linear.easeNone});
							}
						});
					}else if(leave==2 && section==4){
						$('.cat').addClass('walking_right');
						TweenMax.to($('.cat'), 2, {left: '3100px',top: '250px', ease: Linear.easeNone,
							onComplete: function(){
								$('.cat').removeClass('walking_right');
								$('.cat').addClass('active_tax');
								$('.topic_tax').addClass('active');
								$('.broad').css('left','3900px');
								TweenMax.to($('.broad'), 0.5, {delay: 0.5, bottom: '0px', ease: Linear.easeNone});
							}
						});

					}else if(leave==1 && section==2){
						//貓咪從section1向右走到section2
						$('.cat').addClass('walking_right');
						$('.broad').css('bottom','-180px');
						TweenMax.to($('.cat'), 2, {left: '1820px',top: '250px', ease: Linear.easeNone,
							onComplete: function(){
								$('.cat').removeClass('walking_right');
								$('.cat').addClass('active_cost');
								$('.topic_cost').css('left','1550px');
								$('.topic_cost').addClass('active');
								$('.broad').css('left','1050px');
								TweenMax.to($('.broad'), 0.5, {delay: 0.5, bottom: '0px', ease: Linear.easeNone});
							}
						});
					}else if(leave==1 && section==4){
						//貓咪從section1向右走到section4
						$('.cat').addClass('walking_right');
						TweenMax.to($('.cat'), 3, {left: '3100px',top: '250px', ease: Linear.easeNone,
							onComplete: function(){
								$('.cat').removeClass('walking_right');
								$('.cat').addClass('active_tax');
								$('.topic_tax').addClass('active');
								$('.broad').css('left','3900px');
								TweenMax.to($('.broad'), 0.5, {delay: 0.5, bottom: '0px', ease: Linear.easeNone});
							}
						});
					}

					setTimeout(function(){
						$('.all_bg').css('left', bg_move);
						$('.all_area').css('left', area_move);
						$('section').removeClass('active');
						$('section').find('.arrow_control').css('opacity','0');
						$('section').eq(section-1).addClass('active');
						TweenMax.to($('section').eq(section-1).find('.arrow_control'), 0.5, {delay: 1.5, opacity: '1', ease: Linear.easeNone});
					}, 500);
					now = section;
					// console.log('now:'+now);
				}
			}else{
				$('.all_bg, .all_area').removeClass('active');
				if(leave==4 && section==3){
					//貓咪section4向左走到section3
					$('.cat').addClass('walking_left');
					TweenMax.to($('.cat'), 2, {left: '2450px',top: '200px', ease: Linear.easeNone,
						onComplete: function(){
							$('.cat').removeClass('walking_left');
						}
					});
				}else{
					//if(leave==2 && section==3){
					$('.cat').addClass('walking_right');
					TweenMax.to($('.cat'), 2, {left: '2450px',top: '200px', ease: Linear.easeNone,
						onComplete: function(){
							$('.cat').removeClass('walking_right');
						}
					});
				}
				//切換背景
				$('.all_bg').css('left', bg_move);
				$('.all_area').css('left', area_move);
				$('section').removeClass('active');
				$('section').eq(section-1).addClass('active');
				setTimeout(function(){
					$('.all_bg, .all_area').addClass('active');
					$('section').find('.arrow_control').css('opacity','0');
				}, 1000);
				now = section;
				// console.log('now:'+now);
			}
		}
		
}


